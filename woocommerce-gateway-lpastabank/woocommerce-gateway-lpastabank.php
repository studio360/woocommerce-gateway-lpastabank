<?php
/*
Plugin Name: WooCommerce Latvijas Pasta Bank Gateway
Plugin URI: http://portfolio.online-services.org.ua
Description: Extends WooCommerce with an Latvijas Pasta Bank gateway.
Version: 1.0
Author: Zikiy Vadim
Author URI: http://portfolio.online-services.org.ua
Copyright: © 2016 VDZ.
License: GNU General Public License v3.0
License URI: http://www.gnu.org/licenses/gpl-3.0.html
*/

if ( ! defined( 'ABSPATH' ) ) 
{ exit; } // Защита от прямого доступа

//Хук для Woocommerce - регистрируем свой платежный шлюз
add_action('plugins_loaded', 'woocommerce_gateway_lpastabank_init', 0);

function woocommerce_gateway_lpastabank_init() {
        //Если не найден класс шлоюзов Woccommerce
        if ( !class_exists( 'WC_Payment_Gateway' ) )
        { return; }

        //Загружаем класс работы с API Латвийского банка 
        //Обязательно (!) перед классом для работы с Wordpress и WooCommerce
        require_once( plugin_basename( 'classes/ecommerce.php' ) );
	//Загружаем наш класс шлюза класс
        require_once( plugin_basename( 'classes/class-wc-gateway-lpastabank.php' ) );
	
	//Добавляем наш шлюз к Woocommerce
	function woocommerce_add_gateway_lpastabank_gateway($methods) {
		$methods[] = 'WC_Gateway_Lpastabank';
		return $methods;
	}
	
	add_filter('woocommerce_payment_gateways', 'woocommerce_add_gateway_lpastabank_gateway' );
} 