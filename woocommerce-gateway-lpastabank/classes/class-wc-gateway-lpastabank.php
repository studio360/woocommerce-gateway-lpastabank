<?php
/**
 * Оплата через шлюз Latvijas Pasta Bank
 *
 * @class       woocommerce_lpastabank
 * @package     WooCommerce
 * @category    Payment Gateways
 * @author      Zikiy Vadim
 */

class WC_Gateway_Lpastabank extends WC_Payment_Gateway {
    

	/**
	 * Version
	 * @var string
	 */
	public $version = '1.0.0';

	/**
	 * Constructor
	 */
	public function __construct() {
            $this->id = 'lpastabank';
            $this->method_title         = __( 'Latvijas Pasta Bank', 'woocommerce-gateway-lpastabank' );
            $this->method_description   = __( 'Latvijas Pasta Bank Gateway' );
            $this->debug_email 	        = get_option( 'admin_email' );

            // Установка существующих валют используемых банком валюты проверяются в admin_options() с выводом ошибки
            $this->available_currencies = array( 'EUR', 'GBP', 'LTL', 'PLN', 'RUB', 'UAH', 'USD' );

            //Загрузка полей формы
            $this->init_form_fields();

            //Загрузка настроек
            $this->init_settings();
            
            //Получаем значения полей из базы, если сохранили
            $this->enabled = $this->get_option( 'enabled' );
            $this->title = $this->get_option( 'title' );
            $this->description = $this->get_option( 'description' );
            $this->testmode = $this->get_option( 'testmode' );
            $this->merchant_id = $this->get_option( 'merchant_id' );
            $this->merchant_key = $this->get_option( 'merchant_key' );
            $this->gateway_key = $this->get_option( 'gateway_key' );
            $this->key_index = $this->get_option( 'key_index' );
//            $this->debug_email = $this->get_option( 'debug_email' );
            
            //Флаг для определения прямого шлюза оплаты
            $this->has_fields = true;
            
            //Добавляем нашу форму при отправке запроса к API банка  в методе process_payment
            add_action( 'woocommerce_receipt_' . $this->id, array( $this, 'receipt_page' ) );
            //ДОбавляем хук который сохраняет наши поля в базу при сохранении настроек плагина оплаты
            add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
            // Payment listener/API hook - проверка оплаты в методе process_payment
            add_action('woocommerce_api_wc_gateway_' . $this->id, array($this, 'check_payment_response'));
    }

    
    /*
     * Конструктор формы для админки - тестовый режим и рабочий. 
     * Хранят ключи публичные и приватные для рабочей версии и для тестовой
     */
    
    public function init_form_fields() {
    	$this->form_fields = array(
			'enabled' => array(
				'title'       => __( 'Enable/Disable', 'woocommerce-gateway-lpastabank' ),
				'label'       => __( 'Enable Latvijas Pasta Bank Plagin', 'woocommerce-gateway-lpastabank' ),
				'type'        => 'checkbox',
				'description' => __( 'This controls whether or not this gateway is enabled within WooCommerce.', 'woocommerce-gateway-lpastabank' ),
				'default'     => 'yes',
				'desc_tip'    => true
			),
			'title' => array(
				'title'       => __( 'Title', 'woocommerce-gateway-lpastabank' ),
				'type'        => 'text',
				'description' => __( 'This controls the title which the user sees during checkout.', 'woocommerce-gateway-lpastabank' ),
				'default'     => __( 'Latvijas Pasta Bank', 'woocommerce-gateway-lpastabank' ),
				'desc_tip'    => true
			),
			'description' => array(
				'title'       => __( 'Description', 'woocommerce-gateway-lpastabank' ),
				'type'        => 'text',
				'description' => __( 'This controls the description which the user sees during checkout.', 'woocommerce-gateway-lpastabank' ),
				'default'     => '',
				'desc_tip'    => true
			),
			'testmode' => array(
				'title'       => __( 'TEST Latvijas Pasta Bank Sandbox', 'woocommerce-gateway-lpastabank' ),
				'type'        => 'checkbox',
				'description' => __( 'Place the payment gateway in development mode.', 'woocommerce-gateway-lpastabank' ),
				'default'     => 'yes'
			),
			'merchant_id' => array(
				'title'       => __( 'Merchant ID', 'woocommerce-gateway-lpastabank' ),
				'type'        => 'text',
				'description' => __( 'This is the merchant ID, received from Latvijas Pasta Bank.', 'woocommerce-gateway-lpastabank' ),
				'default'     => '3720000'
			),
			'merchant_key' => array(
				'title'       => __( 'Merchant Key', 'woocommerce-gateway-lpastabank' ),
				'type'        => 'textarea',
				'description' => __( 'This is the merchant key, received from Latvijas Pasta Bank.', 'woocommerce-gateway-lpastabank' ),
				'default'     => ''
			),
			'gateway_key' => array(
				'title'       => __( 'Gateway Key', 'woocommerce-gateway-lpastabank' ),
				'type'        => 'textarea',
				'description' => __( 'This is the Gateway Key, received from Latvijas Pasta Bank.', 'woocommerce-gateway-lpastabank' ),
				'default'     => ''
			),
			'key_index' => array(
				'title'       => __( 'INDEX KEY', 'woocommerce-gateway-lpastabank' ),
				'type'        => 'text',
				'description' => __( 'KEY INDEX, received from Latvijas Pasta Bank.', 'woocommerce-gateway-lpastabank' ),
				'default'     => ''
			),
//			'debug_email' => array(
//				'title'       => __( 'Who Receives Debug E-mails?', 'woocommerce-gateway-lpastabank' ),
//				'type'        => 'text',
//				'description' => __( 'The e-mail address to which debugging error e-mails are sent when in test mode.', 'woocommerce-gateway-lpastabank' ),
//				'default'     => get_option( 'admin_email' )
//			)
		);
        
    }
    
    /*    
     * Процесс оплаты и возвращение результатов
     * @since 1.0.0
     */
    function process_payment($order_id)
    {
        $order = new WC_Order($order_id);
        
        //Отправляем на страницу оплаты
        return array(
                    'result' => 'success',
                    'redirect' => add_query_arg('order', $order->id, add_query_arg('key', $order->order_key, get_permalink(woocommerce_get_page_id('pay'))))
        );
        
    }
    
    // Валидация полей формы
    public function validate_fields() {
		return true;
    }
    
    /**
    * Страница с формой оплаты, точнее формой для API Банка
    * Отображает текст и форму для подтверждения оплаты на странице банка
    *
    * @since 1.0.0
    */
    
    public function receipt_page( $order ) {
            echo '<p>' . __( 'Спасибо за Ваш заказ, пожалуйста, нажмите на кнопку ниже, чтобы произвести оплату', 'woocommerce-gateway-lpastabank' ) . '</p>';
            echo $this->generate_lpastabank_form( $order );
            $this->check_payment_response($order);
    }
    
    /**
    * Генератор формы для API Банка + Комментов по платежу
    *
    * @since 1.0.0
    */
    public function generate_lpastabank_form($order_id) {
        global $woocommerce;
        $customer_order  = wc_get_order( $order_id );
        
//        echo '<pre>';
//        print_r($customer_order);
//        echo '</pre>';
        
        //Проверяем тестовый это режим или нет 
        $test_mode = ( $this->testmode == TRUE ) ? 'TRUE' : 'FALSE';

        //Определяем URL API для реального и тестового режимов
        $api_url = ( "FALSE" == $test_mode ) 
                                           ? 'https://demo.ipsp.lv/form/v2/'
                                           : 'https://demo.ipsp.lv/form/v2/';
                
                
                /*CUSTOMER_INFO*/
                $my_order_info = array(
                        // Billing Information
			"billing_first_name"         	=> $customer_order->billing_first_name,
			"billing_last_name"          	=> $customer_order->billing_last_name,
			"billing_address"            	=> $customer_order->billing_address_1,
			"billing_city"              	=> $customer_order->billing_city,
			"billing_state"              	=> $customer_order->billing_state,
			"billing_zip"                	=> $customer_order->billing_postcode,
			"billing_country"            	=> $customer_order->billing_country,
			"billing_phone"              	=> $customer_order->billing_phone,
			"billing_email"              	=> $customer_order->billing_email,
			
			// Shipping Information
			"ship_to_first_name" 	=> $customer_order->shipping_first_name,
			"ship_to_last_name"  	=> $customer_order->shipping_last_name,
			"ship_to_company"    	=> $customer_order->shipping_company,
			"ship_to_address"    	=> $customer_order->shipping_address_1,
			"ship_to_city"       	=> $customer_order->shipping_city,
			"ship_to_country"    	=> $customer_order->shipping_country,
			"ship_to_state"      	=> $customer_order->shipping_state,
			"ship_to_zip"        	=> $customer_order->shipping_postcode,
			
			// Some Customer Information
			"cust_id"            	=> $customer_order->user_id,
			"customer_ip"        	=> $_SERVER['REMOTE_ADDR'],
                    );
                
                //Описания для заказа - включено все значения пользователя
                $my_desc = '';
                foreach ($my_order_info as $key => $value) {
                    $my_desc.=$key . ': ' . $value . "\r\n";
                }
                
                //Описания для заказа - продукты - подходит лучше чем 1й вариант
                $my_desc = 'Покупка: ';
                $products = $customer_order->get_items();
                foreach ($products as $product) {
                    $my_desc .= $product['name'] . "; \r\n";
                }
                $my_desc .= 'ID пользователя: '. $customer_order->user_id;
                
                $configs = array(
                            'my_config' => array(
                                    'merchant_id'	=> trim($this->merchant_id),
                                    'gateway_key'	=> trim($this->gateway_key),
                                    'merchant_key'	=> trim($this->merchant_key),
                                    //Эмулируем ошибку
//                                    'merchant_key'	=> str_replace('1', '5', trim($this->merchant_key)) ,
                                    'key_index'		=> trim($this->key_index)
                            ),
                );
                $mc= new Ecommerce($configs['my_config']);
                $form = new EcommerceFORM($mc);
		// This is where the fun stuff begins
		$fields = $form->getRequest(
                    array(
                            'AutoDeposit' => 'true', // PHP serializes boolean values incorrectly, so send this as string
                            'Payment' => array(
                                    'Mode' => 3
                            ),
                            'Order' => array(
                                    'ID' => 'VDZ_ID_' .$customer_order->id .'_'. microtime(),
                                    'Amount' => (int)$customer_order->order_total*100, // In minor units, thus 100 equals 1.00 EUR//$customer_order->order_total
                                    'Currency' => $customer_order->get_order_currency(),//'USD',
                                    'Description' => $my_desc,
                            ),
                            'Notification' => $my_desc
                    ), array(
                        'Callback' => 'http://' . $_SERVER['SERVER_NAME'] . str_replace(array('&succes=true', '&error=true'),'',$_SERVER['REQUEST_URI']).'&succes=true',
//                        'Callback' => $this->get_return_url( $customer_order ),//Страница спасибо за покупку - если заказ совершон
                        'ErrorCallback' => 'http://' . $_SERVER['SERVER_NAME'] . str_replace(array('&succes=true', '&error=true'),'',$_SERVER['REQUEST_URI']).'&error=true',
                    )
                );
                
        
//        echo '<pre>';
//        print_r($_POST);
//        echo $customer_order->order_total;
//        echo $customer_order->get_order_currency();
//        echo '</pre>';
        $my_form = '';
        $my_form .= '<form action="https://demo.ipsp.lv/form/v2/" method="post">';
	foreach ($fields as $key => $value) { 
		$my_form .= '<input type="hidden" name="'.$key.'" value="'.$value.'" />';
	}
	$my_form .= '<input type="submit" name="SUBMIT" value="Подтвердить" />';
        $my_form .= '</form>';
        
        return $my_form;
        
    }
    
    /**
     * Проверка ответа сервера и сохранение статусов в комментариях к заказу 
     * Задаем флаги для страниц с ошибкой (&error=true) и удачной оплаты (&succes=true)
     * (видно из админки)
     * 
     * @since 1.0.0
     */
    function check_payment_response($order_id)
    {
        global $woocommerce;
        $customer_order = new WC_Order( $order_id );
//        echo '<pre>';
//        print_r($_GET);
//        print_r($_POST);
//        echo '</pre>';
        
        //Проверяем ответ сервера на ошибку //Флаги передаются в генерируемой форме generate_lpastabank_form()
        if(substr_count($_SERVER['REQUEST_URI'], '&error=true'))
        {
            //Добавляем коммент к заказу о ошибке
            $customer_order->add_order_note( __( 'Произошла ошибка оплата не прошла: ', 'woocommerce-gateway-lpastabank' ) . implode('|',$_POST));
            //Выводим сообщение о ошибке для пользователя
            echo '<ul class="woocommerce-error"><li>'.__( 'Произошла ошибка оплата не прошла, попробуйте пожалуйста снова.', 'woocommerce-gateway-lpastabank' ) . '</li></ul>';
//            throw new Exception( __( 'Произошла ошибка оплата не прошла', 'woocommerce-gateway-lpastabank' ) );
		
        }
        //Проверяем ответ сервера //Флаги передаются в генерируемой форме generate_lpastabank_form()
        if ((substr_count($_SERVER['REQUEST_URI'], '&succes=true')) && ($_POST['INTERFACE'] == $this->merchant_id) && ($_POST['KEY_INDEX'] == $this->key_index)) {
            
//            echo '<pre>';
//            print_r($_POST);
//            echo '</pre>';
            $configs = array(
                'my_config' => array(
                    'merchant_id'	=> trim($this->merchant_id),
                    'gateway_key'	=> trim($this->gateway_key),
                    'merchant_key'	=> trim($this->merchant_key),
                    'key_index'		=> trim($this->key_index)
                ),
            );
            try {
                $mc= new Ecommerce($configs['my_config']);
                $form = new EcommerceFORM($mc);
                $server_response = json_decode(json_encode((array)$form->getResponse($_POST)), TRUE);
                
//                echo '<pre>';
//                print_r($server_response);
//                echo '</pre>';
                
                if(isset($server_response['Payment']['ID']))
                {
                    // Уменьшаем уровень запасов на складе
                    $customer_order->reduce_order_stock();

                    // Очищаем корзину
                    $woocommerce->cart->empty_cart();

                    // Отметить заказ как на удержании - ожидаем оплаты
                    $customer_order->update_status('completed', __( 'Оплата прошла успешно', 'woocommerce' ));

                    //Добавляем коммент к заказу если оплата прошла успешно
                    $customer_order->add_order_note( 'Оплата прошла успешно. Оплачено: '.($server_response['Order']['Amount']/100) .$server_response['Order']['Currency'] . ' ID оплаты: '. $server_response['Payment']['ID'] . ' ID заказа: '. $server_response['Order']['ID']);
                }
                
                //Редиректим на страницу спасибо за покупку
                wp_redirect($this->get_return_url( $customer_order ), 301);
                
            } catch (Exception $my_error) {
                echo '<ul class="woocommerce-error"><li>'.__( 'Произошла ошибка оплата не прошла, попробуйте пожалуйста снова.', 'woocommerce-gateway-lpastabank' ) . '</li></ul>';
            }
            
        }
    }
    /**
     * is_valid_for_use() - используется для настроек в админ панели admin_options()
     * 
     * Check if this gateway is enabled and available in the base currency being traded with.
     *
     * @since 1.0.0
     */
	public function is_valid_for_use() {
		$is_available          = false;
                $is_available_currency = in_array( get_woocommerce_currency(), $this->available_currencies );

                        if ( $is_available_currency && $this->merchant_id && $this->merchant_key ) {
                                $is_available = true;
                        }
                return $is_available;
	}

	/**
	 * Admin Panel Options
	 * - Options for bits like 'title' and availability on a country-by-country basis
	 *
	 * @since 1.0.0
	 */
	public function admin_options() {
    	if ( in_array( get_woocommerce_currency(), $this->available_currencies ) ) {
			parent::admin_options();
		} else {
    		?>
			<h3><?php _e( 'Latvijas Pasta Bank', 'woocommerce-gateway-lpastabank' ); ?></h3>
			<div class="inline error"><p><strong><?php _e( 'Gateway Disabled - Сurrencies not available' , 'woocommerce-gateway-lpastabank' ); ?></strong> <?php echo __( 'Latvijas Pasta Bank Gateway' ); ?></p></div>
    		<?php
		}
    }


	/**
	 * Log system processes. - не используется - все пишем в комментах к заказу (доступно из админки)
	 * @since 1.0.0
	 */
	public function log( $message ) {
		if ( 'yes' === $this->get_option( 'testmode' ) ) {
			if ( ! $this->logger ) {
				$this->logger = new WC_Logger();
			}
			$this->logger->add( 'lpastabank', $message );
		}
	}


}
